# replace ls with exa
alias "ls" "exa -lah"
alias "l" "exa -lah"
alias "desktop" "cd ~/Desktop"

# use neovim for vim
alias "vi" "nvim"
alias "vim" "nvim"

# kubectl
alias "k" "kubectl"
alias "kcuc" "kubectl config use-context"
alias "kc" "kubectl create"
alias "kcf" "kubectl create -f"
alias "kd" "kubectl delete"
